package constant

const (
	Version = "1.0.0-SNAPSHOT"

	ChangelogHeader   = "# Changelog\n\n"
	ChangelogFileName = "CHANGELOG.md"

	ReleaseNoteDirectory = "release-notes"
	ReleaseNotestDetails = "details.json"
	ReleaseNotestHeader  = "# Release Notes"
	ReleaseNotesFileName = "RELEASE_NOTES.md"
)
