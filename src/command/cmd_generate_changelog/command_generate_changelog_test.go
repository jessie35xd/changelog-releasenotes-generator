package cmd_generate_changelog

import (
	"testing"
)

func TestCommandGenerateChangelog_Execute(t *testing.T) {
	cmdReleaseNote := &CommandGenerateChangelog{}
	cmdReleaseNote.Execute("../../../")
}

func TestCommandGenerateChangelog_PrintInfo(t *testing.T) {
	cmdReleaseNote := &CommandGenerateChangelog{}
	cmdReleaseNote.PrintInfo()
}
