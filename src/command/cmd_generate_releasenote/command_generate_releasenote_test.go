package cmd_generate_releasenote

import (
	"showmeyourcode/changelog-releasenotes-generator/src/constant"
	"testing"
)

func TestCommandGenerateReleaseNote_Execute(t *testing.T) {
	cmdReleaseNote := &CommandGenerateReleaseNote{FileName: constant.ReleaseNotesFileName}
	cmdReleaseNote.Execute("../../../")
}

func TestCommandGenerateReleaseNote_PrintInfo(t *testing.T) {
	cmdReleaseNote := &CommandGenerateReleaseNote{FileName: constant.ReleaseNotesFileName}
	cmdReleaseNote.PrintInfo()
}
